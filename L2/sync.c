#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

 // Node structure (for each file)
struct ListNode {
   char *fileName;
   char type;
   struct ListNode *next;
};

// List structure for each directory
struct List {
	char *path;
	struct ListNode *head;
};

void syncDirectories (struct List *listA, struct List *listB);
    
// mallocs space for new file
struct ListNode *createFile(char *name, char type) {
	struct ListNode *newNode;
	newNode = malloc(sizeof(struct ListNode));
	char *fileName = malloc(strlen(name));
    strcpy(fileName, name);
	newNode->fileName = fileName;
	newNode->type = type;
    newNode->next = NULL;
	return newNode;
}

// mallocs space for new list
struct List *createList(char *path) {
	struct List *newList;
	newList = malloc(sizeof(struct List));
	char *pathName = malloc(strlen(path));
    strcpy(pathName, path);
    newList->path = pathName;
	newList->head = NULL;
	return newList;
}

// returns a the lower case version of word
char *strLower(char * word){
	// malloc space for copy
	char *newStr = malloc(strlen(word));
	// copy each char
	int i = 0;
	for(i = 0; word[i] != '\0'; i++) {
    	newStr[i] = tolower(word[i]);
    }
  	return newStr;
}

// returns 1 if dir is valid
int isValidDir(char *dir){
	if(strcmp(dir, ".")==0){
		return 0;
	}
	if(strcmp(dir, "..")==0){
		return 0;
	}
	if(strcmp(dir, ".DS_Store")==0){
		return 0;
	}
	return 1;
}

// goes through a dir and adds all files to a list
struct List *getListFromDir(char *pathName){
	struct List *list = createList(pathName);

	// open dir
	DIR *d;
	struct dirent *dir;
	d = opendir(pathName);

	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{ 
			// use name to create file node
			if(isValidDir(dir->d_name) == 1){
				struct ListNode *file = createFile(dir->d_name, dir->d_type);
				
				struct ListNode *node = list->head;
				// if list has no head, add as head
				if(node == NULL){
					list->head = file;
				} else {
					// add file to list in alph order
					struct ListNode *prev = NULL;
					int added = 0;
					while((node != NULL)&&(added==0)){
						if(strcmp(strLower(node->fileName), strLower(file->fileName))>0){
							if(prev == NULL){
								list->head = file;
							} else {
								prev->next = file;	
							}
							file->next = node;
							added = 1;
						}
						prev = node;
						node = node->next;
					}

					if(added == 0){
						prev->next = file;
					}
				}
			}
		}
		closedir(d);
	}
	
	return list;
}

// constructs the full path of a file given it's name and directory path
char *getFullPath(char *fileName, char *path){
	char *fullPath = malloc(strlen(fileName)+strlen(path)+1);
    strcpy(fullPath, path);
    strcat(fullPath, "/");
    strcat(fullPath, fileName);
    return fullPath;
}

// gets the last date the file was modified using stat
time_t getLastModifiedDate(char *file, char *path) {
	char *filePath = getFullPath(file, path);
	struct stat statbuf;
    if (stat(filePath, &statbuf) == -1) {
        perror(filePath);
        exit(1);
    }
    return statbuf.st_mtime;
}

// copies content from a directory to another
void copy(char *fileName, char *from, char *to, char type){
	char * fullPathFrom = getFullPath(fileName, from);
	char * fullPathTo = getFullPath(fileName, to);

	if(type == DT_DIR){ 
		// copy recursively every item in the directory
		mkdir(fullPathTo, 0700);
		struct List *subList = getListFromDir(fullPathFrom);
		struct ListNode *node = malloc(sizeof(struct ListNode));
		node = subList->head;
		while(node != NULL){
			copy(node->fileName, subList->path, fullPathTo, node->type);
			node = node->next;
		}
	} else if(type == DT_LNK){
		char *linkPath = realpath(fullPathFrom, NULL);
    	symlink(linkPath, fullPathTo);
	} else {
		// opens file with read and write permissions
		FILE *src = fopen(fullPathFrom, "r");
		FILE *dest = fopen(fullPathTo, "w");
		char ch;
		while((ch = fgetc(src)) != EOF) {
			fputc(ch, dest);
		}

		fclose(src);
		fclose(dest);
	}
}

// removes content
void delete(char *file, char *path, char type){
	char * fullPath = getFullPath(file, path);
	// removes all files inside a inner directory recursively
	if(type == DT_DIR) {
		struct List *subList = getListFromDir(fullPath);
		struct ListNode *node = malloc(sizeof(struct ListNode));
		node = subList->head;
		while(node != NULL){
			delete(node->fileName, subList->path, node->type);
			node = node->next;
		}
		remove(fullPath);
	} else if(type == DT_LNK){
		unlink(fullPath);
	} else {
		remove(fullPath);
	}
}

// replaces content from a directory to another
void replace(struct List *listA, struct List *listB, struct ListNode *nodeA, struct ListNode *nodeB){
	if((nodeA->type == DT_DIR)&&(nodeB->type == DT_DIR)){
		// recursively sync inner directories
		char * fullPathA = getFullPath(nodeA->fileName, listA->path);
		char * fullPathB = getFullPath(nodeB->fileName, listB->path);
		// get list of files from subDirectories
		struct List *sublistA = getListFromDir(fullPathA);
		struct List *sublistB = getListFromDir(fullPathB);

		syncDirectories(sublistA, sublistB);
	} else {
		// find latest modified file
		time_t mtimeA = getLastModifiedDate(nodeA->fileName, listA->path);
		time_t mtimeB = getLastModifiedDate(nodeB->fileName, listB->path);
		double diff = difftime(mtimeB, mtimeA);
				
		// delete early version and copy new version
		if(diff > 0){
			delete(nodeA->fileName, listA->path, nodeA->type);
			copy(nodeB->fileName, listB->path, listA->path, nodeB->type);

			// print log action
			printf("Replaced file \"%s\" in %s with latest version\n", nodeA->fileName, listA->path);
		} else if(diff < 0){
			delete(nodeB->fileName, listB->path, nodeB->type);
			copy(nodeA->fileName, listA->path, listB->path, nodeA->type);
			// print log action
			printf("Replaced file \"%s\" in %s with latest version\n", nodeB->fileName, listB->path);
		}
	}
}

// goes through both directories comparing files adding to b all contents that exist 
// in a and not previously in b, removing all contents in b and not in a, and 
// replacing files with the latest modified version if existent in both a and b.
void syncDirectories (struct List *listA, struct List *listB){
	struct ListNode *nodeA = listA->head;
	struct ListNode *nodeB = listB->head;

	// go through all files
	while(nodeA != NULL){
		if(nodeB==NULL){
			// copy to b remaining files in a, since they don't exist in b
			copy(nodeA->fileName, listA->path, listB->path, nodeA->type);
			// print log action
			printf("Copied file \"%s\" from %s to %s\n", nodeA->fileName, listA->path, listB->path);
			nodeA = nodeA->next;
		} else {
			int cmp = strcmp(nodeA->fileName,nodeB->fileName);
			if (cmp == 0){
				replace(listA, listB, nodeA, nodeB);
				nodeA = nodeA->next;
				nodeB = nodeB->next;
			} else if(cmp < 0){
				// copy file to b
				copy(nodeA->fileName, listA->path, listB->path, nodeA->type);
				// print log action
				printf("Copied file \"%s\" from %s to %s\n", nodeA->fileName, listA->path, listB->path);
				nodeA = nodeA->next;	
			} else {
				// delete file from b since it does not exist in a
				delete(nodeB->fileName, listB->path, nodeB->type);
				// print log action
				printf("Removed \"%s\" from %s\n", nodeB->fileName, listB->path);
				nodeB = nodeB->next;
			}
		}
	}

	// remove remaining files in b, since they don't exist in a
	while(nodeB != NULL){
		delete(nodeB->fileName, listB->path, nodeB->type);
		// print log action
		printf("Removed \"%s\" from %s\n", nodeB->fileName, listB->path);
		nodeB = nodeB->next;	
	}
};

int isInsideDirectory(char *dirA, char *dirB){
	if(strstr(dirB, "/") != NULL){
		if (strstr(dirB, dirA) != NULL){
			return 1;
		}
	}
	return 0;
}

int main(int argc, char *argv[]) {

	// set default directories
	char pathA[128] = "testDirA";
	char pathB[128] = "testDirB";
	
	// read input directories
	if(argc > 1){
		strcpy(pathA, argv[1]);
	}
	if(argc > 2){
		strcpy(pathB, argv[2]);
	} 

	if((isInsideDirectory(pathA, pathB)==1) || (isInsideDirectory(pathB, pathA)==1)){
		printf("Invalid directories: one cannot be inside the other.\n");
	} else {
		// get list of files from directories
		struct List *listA = getListFromDir(pathA);
		struct List *listB = getListFromDir(pathB);

		if((listA->head==NULL)&&(listB->head==NULL)){
			printf("No such directories exist\n");
		}

		// sync directories
		syncDirectories(listA, listB);
	}
	

	return(0);
}