/*
Mariana Quinde Garcia 
CS283-001
Lab 1.1
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h> 

/* 
 Defines int* pointer, creates an array 
 of 10, assigns values, prints and frees them
 */
void part1(){
	// allocate space for array
	int *intPtr;
	intPtr = malloc(sizeof(int)*10);
	if(intPtr == NULL){
		printf("Memory Error\n");
		exit(-1);
	}
	// asign values
	for(int i = 0; i<10; i++){
		intPtr[i] = i;
	}
	// print array
	for(int i = 0; i<10; i++){
		printf("%d \n", intPtr[i]);
	}
	// free memory
	free(intPtr); 
}

 /*
 Creates a char** pointer with char*'s, 
 initializes each of the 10 char*'s to a 
 char array of size 15, initialize each to 
 Hello World! and prints them
 */
void part2(){
	// allocate memory for array of pointers
	char **charPtr;
	charPtr = malloc(sizeof(char *)*10);
	if(charPtr == NULL){
		printf("Memory Error\n");
		exit(-1);
	}
	// allocates memory for each pointer
	for(int i = 0; i<10; i++){
		charPtr[i] = malloc(sizeof(char)*15);
		if(charPtr[i] == NULL){
			printf("Memory Error\n");
			exit(-1);
		}
	}

	// sets char array to hello world
	charPtr[0] = "1  Hello World!!\0";
	charPtr[1] = "2  Hello World!!\0";
	charPtr[2] = "3  Hello World!!\0";
	charPtr[3] = "4  Hello World!!\0";
	charPtr[4] = "5  Hello World!!\0";
	charPtr[5] = "6  Hello World!!\0";
	charPtr[6] = "7  Hello World!!\0";
	charPtr[7] = "8  Hello World!!\0";
	charPtr[8] = "9  Hello World!!\0";
	charPtr[9] = "10  Hello World!\0";
	

	// prints
	for(int i = 0; i<10; i++){
		printf("%s \n", charPtr[i]);
	}
	// frees memory
	for(int i = 0; i<10; i++){
		charPtr[i] = NULL;
		free(charPtr[i]);
	}
	free(charPtr);
}

 /*
 Swaps the element in the array pointed by ptr
 at index x with the following element
 */
void swap1(int *ptr, int x){
	int temp;
	temp = ptr[x];
    ptr[x]  = ptr[x+1];
    ptr[x+1] = temp;
}

/*
Sorts array at intPtr through bubble sort
*/
void sort1(int *intPtr, int size){
	for(int i=0; i<(size-1); i++){
		for (int j=0; j<size-i-1; j++){
			if (intPtr[j] > intPtr[j+1]){
				swap1(intPtr, j);
			}
		}
	}
}

/*
Creates int array and calls sort to sort it.
Prints sorted and unsorted array.
*/
void part3()
{
	// allocate memory
	int *sortPtr;
	sortPtr = malloc(sizeof(int)*10);
	if(sortPtr == NULL){
		printf("Memory Error\n");
		exit(-1);
	}
	// init array
	sortPtr[9] = 1;
	sortPtr[7] = 2;
	sortPtr[4] = 3;
	sortPtr[8] = 4;
	sortPtr[2] = 5;
	sortPtr[0] = 6;
	sortPtr[5] = 7;
	sortPtr[3] = 8;
	sortPtr[1] = 9;
	sortPtr[6] = 10;

	// print before
	printf("Before:\n");
	for(int i = 0; i<10; i++){
		printf("%d \n", sortPtr[i]);
	}

	// sort
	sort1(sortPtr, 10);

	// print after
	printf("After:\n");
	for(int i = 0; i<10; i++){
		printf("%d \n", sortPtr[i]);
	}

	free(sortPtr);
	
}

 // List and node structures
struct List {
	struct ListNode* head;
};

struct ListNode {
   int dataElement;
   struct ListNode* next;
};

/*
 Swaps the element in the linked list
 at index x with the following element
 */
void swap2(struct ListNode* prev, struct ListNode* node, struct ListNode* next){
	// store element
	struct ListNode temp;
	temp = *node;

	//swap
    *node = *next;
    *next = temp;

	// swap next values of each element
    next->next = node ->next;
    node->next = next;

    // updated next value of prev node, if existent
    if (prev!=NULL){
    	prev->next = node;
    }
}

/*
Sorts linked list through bubble sort
*/
void sort2(struct ListNode* head){
	// counter for first loop
	struct ListNode* counter = head;
	if (head->next != NULL){
		while(counter->next != NULL){
			struct ListNode* prev = NULL;
			struct ListNode* node = head;
			struct ListNode* next = node->next;
			// go through list again
			while(next != NULL){
				// compare ints and swap
				if (node->dataElement > next->dataElement){
					swap2(prev, node, next);
				}
				prev = node;
				node = next;
				next = next->next;
			}
			counter = counter->next;
		}		
	}
}

/*
Creates linked list and calls sort to sort it.
Prints sorted and unsorted array.
*/
void part4()
{
	// init nodes
	struct ListNode node1 = {
		.dataElement= 1
	};
	struct ListNode node2 = {
		.dataElement= 2
	};
	struct ListNode node3 = {
		.dataElement= 3
	};
	struct ListNode node4 = {
		.dataElement= 4
	};
	struct ListNode node5 = {
		.dataElement= 5
	};
	struct ListNode node6 = {
		.dataElement= 6
	};
	struct ListNode node7 = {
		.dataElement= 7
	};
	struct ListNode node8 = {
		.dataElement= 8
	};
	struct ListNode node9 = {
		.dataElement= 9
	};
	struct ListNode node10 = {
		.dataElement= 10
	};

	// init list and set head
	struct List list = {
		.head= &node5
	};

	// set nexts
	node5.next = &node10;
	node10.next = &node2;
	node2.next = &node8;
	node8.next = &node4;
	node4.next = &node7;
	node7.next = &node1;
	node1.next = &node6;
	node6.next = &node9;
	node9.next = &node3;

	// print unsorted
	printf("Before:\n");
	struct ListNode* node = list.head;
	while(node != NULL){
		printf("%d \n", node->dataElement);
		node = node->next;
	}	
	
	sort2(list.head);
	
	// print sorted
	node = list.head;
	printf("After:\n");
	while(node != NULL){
		printf("%d \n", node->dataElement);
		node = node->next;
	}
	
}

/*
Adds element to array in index 
Uses realloc to resize
*/
void addElement(int* ptr, int element, int index, int size){	
	// add
	for (int i = index; i < size+1; i++) {
		int temp = ptr[index];
		ptr[index] = element;
		element = temp;
	}
	
}

/*
Removes element at index 
Uses realloc to resize
*/
void removeElement(int* ptr, int index, int size){
	for (int i = index; i < size-1; i++) {
		ptr[i] = ptr[i+1];
	}
}

/*
Gets element at index
*/
int get(int* ptr, int index){
	return ptr[index];
}

/*
Creates an array os size n, inits all values, prints,
removes first value, prints, adds one element,
print, adds 100000 elements, prints time elapsed
*/
void part5a(int argc, char *argv[])
{
	int size = 10;
	if(argc > 1){
		size = atoi(argv[1]);
	}

	printf("Initial size: %d:\n", size);
	// allocates memory
	int *ptr;
	ptr = (int *)malloc(sizeof(int)*size);
	if(ptr == NULL){
		printf("Memory Error\n");
		exit(-1);
	}
	// inits values
	for(int i = 0; i<size; i++){
		ptr[i] = i;
	}
	
	// prints
	for(int i = 0; i<size; i++){
		printf("%d \n", get(ptr, i));
	}

	// removes element
	if (size!=0){
		removeElement(ptr, 0, size);
		ptr = realloc(ptr, sizeof(int)*(size-1));
		if(ptr == NULL){
			printf("Memory Error\n");
			exit(-1);
		}
		// prints
		printf("After removing first element:\n");
		size = size - 1;
		for(int i = 0; i<size; i++){
			printf("%d \n", get(ptr, i));
		}
	}


	// adds one element	
	printf("After adding one element:\n");
	// realloc
	ptr = (int *)realloc(ptr, sizeof(int)*(size+1)); 
	if(ptr == NULL){
		printf("Memory Error\n");
		exit(-1);
	}
	addElement(ptr, size+1, size, size);
	size = size + 1;
	// prints
	for(int i = 0; i<size; i++){
		printf("%d \n", get(ptr, i));
	}

	// add 100000 and time it
	clock_t start = clock();
	for (int i = size; i < 100000; i++){
		// realloc
		ptr = (int *)realloc(ptr, sizeof(int)*(size+1)); 
		if(ptr == NULL){
			printf("Memory Error\n");
			exit(-1);
		}
		addElement(ptr, i+1, size, size);
		size++;
	}
	clock_t diff = clock() - start;
	float sec = ((float)(diff)/CLOCKS_PER_SEC)*1000;
	printf("Miliseconds (CPU time) taken to add 100000 at the end of array: %f \n", sec);
	free(ptr); 
	
}

/*
Creates an array os size n, inits all values, 
adds 200000 elements, prints time elapsed
*/
void part5b(int argc, char *argv[])
{
	int size = 10;
	if(argc > 1){
		size = atoi(argv[1]);
	}

	// allocates memory
	int *ptr;
	ptr = (int *)malloc(sizeof(int)*size);
	if(ptr == NULL){
		printf("Memory Error\n");
		exit(-1);
	}
	// inits values
	for(int i = 0; i<size; i++){
		ptr[i] = i;
	}
	
	// add 200000 and time it
	clock_t start = clock();
	for (int i = size; i < 200000; i++){
		// realloc
		ptr = (int *)realloc(ptr, sizeof(int)*(size+1)); 
		if(ptr == NULL){
			printf("Memory Error\n");
			exit(-1);
		}
		addElement(ptr, i+1, size, size);
		size++;
	}
	clock_t diff = clock() - start;
	float sec = ((float)(diff)/CLOCKS_PER_SEC)*1000;
	printf("Miliseconds (CPU time) taken to add 200000 at the end of array: %f \n", sec);

	free(ptr); 
	
}

 int main(int argc, char *argv[])
{
	printf("Part 1:\n");
	part1();
	printf("\n\n\n\n----------\nPart 2:\n");
	part2();
	printf("\n\n\n\n----------\nPart 3:\n");
	part3();
	printf("\n\n\n\n----------\nPart 4:\n");
	part4();
	printf("\n\n\n\n----------\nPart 5:\n");
	part5a(argc, argv);
	part5b(argc, argv);
}