/*
Mariana Quinde Garcia 
CS283-001
L4
*/

#include <pthread.h>
#include <stdio.h>
#include <time.h>

volatile unsigned int counter = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
#define TIMES 1000

// threaded funtion invoked via threads
// increments a shared volatile variable
// 1000 times per thread with a mutex lock
void *threadedFun(void *arg) {
    int i;
    pthread_mutex_lock( &mutex );
    for (i=0; i<TIMES; i++) {
        counter++;
    }
    pthread_mutex_unlock(&mutex);
    return NULL;
}

// creates 100 threads (people)
// and calls the threaded function
// via each one. It displays the time
// and the total times people have gotten up
// (counter)
int main() {
    printf("-- Part 3 --\n");
    pthread_t threads[100];
    clock_t start, end;

    int i;
    start = clock();
    for (i = 0; i < 100; ++i) {
        pthread_create(&(threads[i]), NULL, threadedFun, NULL);
    }

    for (i = 0; i < 100; ++i) {
        pthread_join(threads[i], NULL);
    }
    end = clock();
    
    // display results
    if (counter != (unsigned)TIMES*100) {
        printf("Improper Synchronization");
    }
    
    printf("Times people got up: %d\n", counter);

    printf("Time taken: %fs\n", ((double) (end - start))/CLOCKS_PER_SEC);

    return 0;
}