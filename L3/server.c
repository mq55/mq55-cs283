/*
Mariana Quinde Garcia 
CS283-001
L3
*/

#include "csapp.h" 

// returns the file requested by client
FILE *getFile(int connfd){
    char *path, buf[MAXLINE];
    FILE *file;
    rio_t rio; 
    
    // read request from client
    Rio_readinitb(&rio, connfd);
    Rio_readlineb(&rio, buf, MAXLINE);
    
    // get the file path from request
    if (&buf[0] != NULL) {
        path = strtok(buf,"GET /");
        file = fopen(path,"r");
    }

    return file;
}

// returns the contents of the file to client
void returnFile (FILE *file, int connfd){
    char line[MAXLINE];
    // read each line and send to client
    while(fgets(line, sizeof(line), file) != NULL) {
        Rio_writen(connfd, line, strlen(line));
    }
}

int main(int argc, char **argv) {
    int listenfd, connfd, port, clientlen;
    struct sockaddr_in clientaddr;
    struct hostent *hp;
    char *haddrp;

    // get port
    port = 80;
    if(argc > 0){
        port = atoi(argv[1]);
    } 

    // start listening
    listenfd = open_listenfd(port);
    while (1) {
        // listen
        clientlen = sizeof(clientaddr); 
        connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
        // get host data
        hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
                        sizeof(clientaddr.sin_addr.s_addr), AF_INET);
        haddrp = inet_ntoa(clientaddr.sin_addr);
        // get file and send to client
        FILE *file = getFile(connfd);
        returnFile(file, connfd);
        // close connection
        Close(connfd);
    }
}
