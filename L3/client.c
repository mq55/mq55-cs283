/*
Mariana Quinde Garcia 
CS283-001
L3
*/

#include "csapp.h" 

// function to concat two strings
char* concatStr(char *a, char *b)
{
    char *concat = malloc(strlen(a)+strlen(b)+1);
    strcpy(concat, a);
    strcat(concat, b);
    return concat;
}

int main(int argc, char **argv)
{ 
    // info for reaching to server
    int clientfd, port, contentLength;
    // strings for request and for return data
    char *host, *page, *request, *delimiter, *cLength, *token, buf[MAXLINE];
    // store server data size
    size_t n;

    rio_t rio; 
    
    // get passed values
    page = "index.html";
    if(argc > 1){
        page = malloc(strlen(argv[1])+1);
        strcpy(page, argv[1]);
    } 
    host = "www.google.com";
    if(argc > 2){
        host = malloc(strlen(argv[2])+1);
        strcpy(host, argv[2]);
    } 
    port = 80;
    if(argc > 3){
        port = atoi(argv[3]);
    } 

    // build request from arguments
    request = concatStr("GET /", page);
    request = concatStr(request, " HTTP/1.1\r\nHost: ");
    request = concatStr(request, host);
    request = concatStr(request, "\r\n\r\n");
    
    // show request 
    printf("Request: %s\n", request);

    // open, send message and receive lines from server
    clientfd = Open_clientfd(host, port);
    Rio_readinitb(&rio, clientfd);
    strcpy(buf, request);
    Rio_writen(clientfd, buf, strlen(buf));
    cLength = "Content-Length: ";
    delimiter = "\r\n";
    contentLength = -1;
    // read headers to get content length
    while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) { 
        Fputs(buf, stdout);
        // check if the \r\n delimiter has been read twice so that no overeading
        // is done (header and response)
        if(strstr(buf, cLength) != 0){
            token = strtok(buf, cLength);
            contentLength = atoi(token);
        }
        if(strcmp(buf, delimiter) == 0){
            break;
        }
    }

    // if the content length was found, update readlineb length
    if (contentLength == -1){
        contentLength = MAXLINE;
    }
    
    // read the rest of the request
    n = Rio_readlineb(&rio, buf, contentLength);
    while (n > 0) {
        printf("%s", buf);
        n = Rio_readlineb(&rio, buf, contentLength);
    }
    printf("\n");
    // close connection
    Close(clientfd); 
    exit(0);
}
