'''
Mariana Quinde Garcia 
Stevie Parris
CS283-001
L5
'''
import csv
import sys
import time
from multiprocessing import Pool
import numpy as np

# --------------------Sequentially--------------------------
def sequentially():
	print "Calculating Sequentially..."
	# start time
	startTime = time.time()
	data = dict()
	# open csv file
	with open('otp.csv', 'rb') as csvfile:    
	    csvreader = csv.DictReader(csvfile)
	    # read each row in file  
	    for row in csvreader: 
	    	# parse id
	    	id_str = (row['train_id']).replace(".","")
	    	if id_str.isdigit():
	    		line_id = (int(id_str) % 1000)/100
	    		# parse status
	    		status_str = row['status']
	    		status = 0.0
	    		if status_str != 'On Time':
	    			split = status_str.split(' ')
	    			status = float(split[0])
	    		# add info to data dictionary
	    		if not (line_id in data):            
	        		data[line_id] = [0,0]
        		data[line_id][0] += 1
        		# if within time frame, increase counter
        		if status <= 5:
        			data[line_id][1] += 1

    # print data per row
	for line_id in data:
		# calculate percentage
		percentage = (data[line_id][1]/float(data[line_id][0]))*100
		print int(percentage), "% of trains in line", line_id, "are within 5 mins on time"
	
	# print time
	print "Time:", (time.time() - startTime), " seconds"
#--------------------------MapReduce--------------------------

# Taken from lecture in week 7
# gets val in csv 
def getValByKey(row, col):    
    global header 
    result = ''    
    idx = 0    
    
    for keycol in header.split(','):        
        if keycol == col:            
            result = row.split(',')[idx]            
            break        
        idx = idx + 1    

    result = result.strip().replace('\'', '')    
    return result

# Taken from lecture in week 7
# organizes into one dictionary
def partition(mappings_list):    
    # mappings is an array of dicts of line_id keys to status values, 
    # which we want to aggregate into a single dict organized by key  
    result = dict()    
    for mappings in mappings_list:        
        for key in mappings:            
            for val in mappings[key]:                
                if not (key in result):
                    result[key] = []                

                result[key].append(val)
    return result

# Makes a dictionary with the line_id's and statuses list
def mapper(process_data):    
    result = dict()
    # parse data
    for row in process_data:        
        key = getValByKey(row, 'train_id')        
        id_str = (key.replace(".",""))
        # parse line_id
        if id_str.isdigit():
        	line_id = (int(id_str) % 1000)/100
        	if not (line_id in result):
        		result[line_id] = []        
        	# parse status
        	status_str = getValByKey(row, 'status')
    		status = 0.0
    		if status_str != 'On Time':
    			split = status_str.split(' ')
    			status = float(split[0])
    		# add to line_id entry        
        	result[line_id].append(status)  

    return result

# Takes a tuple with a line_id and a status list and computes percentage
def reducer(tuple):    
    data = dict()    
    counter = 0.0
    within = 0.0
    # add per status to counters
    for status in tuple[1]:
        counter += 1
        if status <= 5:
        	within += 1
    # compute percentade
    percentage = (within/counter)*100
    return [tuple[0],percentage]


header = ''
# if nothing passed, run sequentially
if len(sys.argv) <= 1:
	sequentially()
else:
	# get number of nodes and start map reduce
	startTime = time.time()
	processes = int(sys.argv[1])
	csvlines = []
	csvfile = open('otp.csv', 'r')
	lineno = 0
	print "Calculating In Parallel With", processes, "Nodes..."
	# Taken from lecture in week 7
	# read file and store lines
	for line in csvfile:    
	    if lineno > 0:        
	        csvlines.append(line)    
	    else:        
	        header = line    
	    lineno = lineno + 1

	# Taken from lecture in week 7
	# start a process with a number of lines(depending on nodes)
	numlines = len(csvlines)
	lines_per_process = numlines / processes
	process_data_array = []
	# break up lines into an array of "processes" elements, 
	# each element containing an array of the N'th cluster of numlines / processes lines of text
	# again, Hadoop would do this for us as part of HDFS
	#process_data_array = []
	for i in range(processes):    
	    start = i * (numlines / processes)  
	    end = (i+1) * (numlines / processes)    
	    process_data_array.append(csvlines[start:end])

	pool = Pool(processes=processes)

	# Run the MapReduce process of map, shuffle, reduce 
	# as well as the partition shuffler function, which is normally handled automatically by HDFS!
	mapping = pool.map(mapper, process_data_array)    
	shuffled = partition(mapping) # shuffled is an array of mapping dicts
	reduced = pool.map(reducer, shuffled.items()) # items is a list of (key, value) tuples

	# Print results
	for line_id in reduced:
	    print int(line_id[1]), "% of trains in line", line_id[0], "are within 5 mins on time"

	print "Time:", (time.time() - startTime), " seconds"

