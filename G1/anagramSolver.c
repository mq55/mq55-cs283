/*
Mariana Quinde Garcia 
CS283-001
G1
*/

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

 // Node structure (for each word)
struct ListNode {
   char *word;
   struct ListNode *next;
};

// Pair Structure (for each combination of letters)
struct Pair {
	int hashKey;
	struct ListNode *head;
	struct Pair *next;
};

// Hash Structure for all dictionary
struct HashTable {
	struct Pair *head;	
};

// mallocs space for hash table
struct HashTable *createNewHash() {
	struct HashTable *newHash;
	newHash = malloc(sizeof(struct HashTable));
	newHash->head = NULL;
	return newHash;
}

// mallocs space for new hash entry
struct Pair *createNewPair(int key) {
	struct Pair *newPair;
	newPair = malloc(sizeof(struct Pair));
	newPair->hashKey = key;
	newPair->head = NULL;
	newPair->next = NULL;
	return newPair;
}

// mallocs space for new word in entry
struct ListNode *createNewNode(char *word) {
	struct ListNode *newNode;
	newNode = malloc(sizeof(struct ListNode));
	newNode->word = word;
	newNode->next = NULL;
	return newNode;
}

// returns a the lower case version of word
char *strLower(char * word){
	// malloc space for copy
	char *newStr = malloc(strlen(word));
	// copy each char
	int i = 0;
	for(i = 0; word[i] != '\0'; i++) {
    	newStr[i] = tolower(word[i]);
    }
  	return newStr;
}

// adds the newNode to the front of the linkedlist
void addToList(struct Pair *pair, struct ListNode * newNode){
	struct ListNode *prev = NULL;
	struct ListNode *node = pair->head;
	pair->head = newNode;
	newNode->next = node;
}

// calculates the hashkey for a word
int getHashKey(char *word){
	int newHashKey = 0;
	int i = 0;
	for(i = 0; word[i] != '\0'; i++) {
    	int val = tolower(word[i]) - 96;
    	// ignore non a-zA-Z to calculate value
    	if((val>0)&&(val<27)) {
    		newHashKey += val;
    	}
    }
    return newHashKey;
}

// adds a word to the correct pair according to its hashkey 
void addToTable(struct HashTable *hash, char *word){
	struct ListNode *newNode = createNewNode(word);
	int newHashKey = getHashKey(word);
	struct Pair *pair = hash->head;
	
	if (pair != NULL){
		struct Pair *prev = NULL;
		while(pair != NULL){
			// add to existing entry if hashKey matches
			if (pair->hashKey == newHashKey){
				addToList(pair, newNode); 
				return;
			}

			// if no entry exists for hashkey, create entry and add it
			if (pair->hashKey > newHashKey) {
				struct Pair *newPair = createNewPair(newHashKey);
				newPair->head = newNode;
				if (prev == NULL){
					hash->head = newPair;
				} else {
					prev->next = newPair;
				}
				newPair->next = pair;
				return;
			}
			prev = pair;
			pair = pair->next;
		}

		// if no entry exists for hashkey, create entry and add it
		struct Pair *newPair = createNewPair(newHashKey);
		newPair->head = newNode;
		prev->next = newPair;
	} else {
		// if hash has no head, create entry and add it
		struct Pair *newPair = createNewPair(newHashKey);
		newPair->head = newNode;
		hash->head = newPair;
	}
}

// prints all entries in hash table
void printHash(struct HashTable *hash){
	struct Pair *pair = hash->head;
    while(pair != NULL){
		// print hash key
		printf("%d: ", pair->hashKey);

		// print list of words
		struct ListNode *node = pair->head;
		while(node != NULL){
			printf("%s ", node->word);
			node = node->next;
		}

		pair = pair->next;
		printf("\n");
	}
}

// checks if word1 is a false positive anagram of word2
int isFalsePositive(char *word1, char *word2) {
	// check if strings are the same length
	if(strlen(word1)!=strlen(word2)){
		return 1;
	}
	// check if all chars in 1 exist in 2
	int i = 0;
	for(i = 0; i<strlen(word1); i++){
		if(strchr(strLower(word2), tolower(word1[i])) == NULL){
			return 1;
		}
	}
	return 0;
}

// print all the anagrams for word stored in hash
void printAnagrams(struct HashTable *hash, char *word){
	int key = getHashKey(word);
	int foundAnagram = 0;
	struct Pair *pair = hash->head;
    while(pair != NULL){
    	// compare hash keys, if found print
		if(pair->hashKey == key) {
			struct ListNode *node = pair->head;
			while(node != NULL){
				// check each word for false positives
				if(!isFalsePositive(node->word, word)){
					printf("%s ", node->word);
					foundAnagram = 1;
				}
				node = node->next;
			}

			// print if no word was found
			if(foundAnagram==0) {
				printf("No anagrams stored for word %s\n", word);
			}
			return;
		}

		// print if no word was found with the same hash key
		if (pair->hashKey > key){
			printf("No anagrams stored for word %s\n", word);
			return;
		}
		pair = pair->next;
	}
}

// creates a hash table with all words in dictionary file
// prints anagram for argument word, or for "lame" by default
int main(int argc, char *argv[]) {
	char inputWord[128]="lame";
	if(argc > 1){
		strcpy(inputWord, argv[1]);
	} 
	printf("Looking for anagrams for %s...\n", inputWord);
	struct HashTable *hash = createNewHash();
    char *filePath = "/usr/share/dict/words";
    // read file
    FILE* dict = fopen(filePath, "r");
    if(dict == NULL) {
    	printf("Dictionary file not found\n");
        return 0;
    }

    char word[128];
    while(fgets(word, sizeof(word), dict) != NULL) {
    	if(strlen(word)>0){
    		// store copy of word
    		char *newWord = malloc(strlen(word));
    		strcpy(newWord, word);
    		addToTable(hash, strtok(newWord, "\n"));
    	}
    }

    
	printAnagrams(hash, inputWord);
	printf("\n");
    return 0;
}
