/* 
 * tsh - A tiny shell program with job control
 * 
 * Mariana Quinde Garcia mq55
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>

/* Misc manifest constants */
#define MAXLINE    1024   /* max line size */
#define MAXARGS     128   /* max args on a command line */
#define MAXJOBS      16   /* max jobs at any point in time */
#define MAXJID    1<<16   /* max job ID */

/* Job states */
#define UNDEF 0 /* undefined */
#define FG 1    /* running in foreground */
#define BG 2    /* running in background */
#define ST 3    /* stopped */

/* 
 * Jobs states: FG (foreground), BG (background), ST (stopped)
 * Job state transitions and enabling actions:
 *     FG -> ST  : ctrl-z
 *     ST -> FG  : fg command
 *     ST -> BG  : bg command
 *     BG -> FG  : fg command
 * At most 1 job can be in the FG state.
 */

/* Global variables */
extern char **environ;      /* defined in libc */
char prompt[] = "tsh> ";    /* command line prompt (DO NOT CHANGE) */
int verbose = 0;            /* if true, print additional output */
int nextjid = 1;            /* next job ID to allocate */
char sbuf[MAXLINE];         /* for composing sprintf messages */

struct job_t {              /* The job struct */
    pid_t pid;              /* job PID */
    int jid;                /* job ID [1, 2, ...] */
    int state;              /* UNDEF, BG, FG, or ST */
    char cmdline[MAXLINE];  /* command line */
};
struct job_t jobs[MAXJOBS]; /* The job list */
/* End global variables */


/* Function prototypes */

/* Here are the functions that you will implement */
void eval(char *cmdline);
int builtin_cmd(char **argv);
void do_bgfg(char **argv);
void waitfg(pid_t pid);

void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);

/* Here are helper routines that we've provided for you */
int parseline(const char *cmdline, char **argv, int *argc); 
void sigquit_handler(int sig);

void clearjob(struct job_t *job);
void initjobs(struct job_t *jobs);
int maxjid(struct job_t *jobs); 
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline);
int deletejob(struct job_t *jobs, pid_t pid); 
pid_t fgpid(struct job_t *jobs);
struct job_t *getjobpid(struct job_t *jobs, pid_t pid);
struct job_t *getjobjid(struct job_t *jobs, int jid); 
int pid2jid(pid_t pid); 
void listjobs(struct job_t *jobs);

void usage(void);
void unix_error(char *msg);
void app_error(char *msg);
typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);

/*
 * main - The shell's main routine 
 */
int main(int argc, char **argv) 
{
    char c;
    char cmdline[MAXLINE];
    int emit_prompt = 1; /* emit prompt (default) */

    /* Redirect stderr to stdout (so that driver will get all output
     * on the pipe connected to stdout) */
    dup2(1, 2);

    /* Parse the command line */
    while ((c = getopt(argc, argv, "hvp")) != EOF) {
        switch (c) {
        case 'h':             /* print help message */
            usage();
	    break;
        case 'v':             /* emit additional diagnostic info */
            verbose = 1;
	    break;
        case 'p':             /* don't print a prompt */
            emit_prompt = 0;  /* handy for automatic testing */
	    break;
        default:
            usage();
        }
    }

    /* Install the signal handlers */

    /* These are the ones you will need to implement */
    Signal(SIGINT,  sigint_handler);   /* ctrl-c */
    Signal(SIGTSTP, sigtstp_handler);  /* ctrl-z */
    Signal(SIGCHLD, sigchld_handler);  /* Terminated or stopped child */

    /* This one provides a clean way to kill the shell */
    Signal(SIGQUIT, sigquit_handler); 

    /* Initialize the job list */
    initjobs(jobs);

    /* Execute the shell's read/eval loop */
    while (1) {

	/* Read command line */
	if (emit_prompt) {
	    printf("%s", prompt);
	    fflush(stdout);
	}
	if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
	    app_error("fgets error");
	if (feof(stdin)) { /* End of file (ctrl-d) */
	    fflush(stdout);
	    exit(0);
	}

	/* Evaluate the command line */
	eval(cmdline);
	fflush(stdout);
	fflush(stdout);
    } 

    exit(0); /* control never reaches here */
}
  
/* 
 * eval - Evaluate the command line that the user has just typed in
 * 
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately. Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.  
*/
void eval(char *cmdline) 
{
    pid_t pid;
    pid_t pidPipe;
    
    // dignal set
    sigset_t set;

    // pointers for commands
    char **argv = (char **) malloc(sizeof(char *) * MAXLINE);
    char **argvPipe = NULL;

    // pipes for redirection
    int fd[2];
    
    // file for redirection
    int out = 0;
    
    // number of args
    int argc = 0;
    
    //block SIGCHLD signals before forking the child to avoid race conditions
    sigprocmask(SIG_BLOCK, &set, NULL);

    // get parsed commands
    int bg = parseline(cmdline, argv, &argc);

    // check for null args
    if(argv[0] == NULL){
        return;
    }

    // check if they are built in commands 
    int i = builtin_cmd(argv);

    // not a built in command
    if(i==0) {
        // start signal set
        sigemptyset(&set);
        sigaddset(&set, SIGCHLD);
        
        // check for ouput redirections
        int j;
        for(j = 0; j < argc ; j++){
            // find command for redirection
            if(*argv[j] == '>') {
                // open file with write permission
                out = open(argv[j+1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
                if(out < 0){
                    perror("File not found");
                }
                // remove end of command
                argv[j] = NULL;
                argc = argc - 2;
                j = argc;
            }
        }

        // check for pipelines
        for(j = 0; j < argc ; j++){
            if(*argv[j] == '|') {
                // Open pipe
                int p = pipe(fd);
                if(p < 0){
                    unix_error("could not open pipe");
                }
                // use rest of command as second command
                argvPipe = (char **) malloc(sizeof(char *) * MAXLINE);
                argvPipe = &(argv[j+1]);
                // remove last values
                argv[j] = NULL;
                j = argc;
            }
        }

        
        pid = fork();
        if(pid < 0){
            unix_error("fork");
        }
        if (pid == 0) {
            // unblock for execution
            sigprocmask(SIG_UNBLOCK, &set, NULL);            
            
            // Put child in new process group with same id as child's PID to separate from shell group
            setpgid(0,0);
            
            if(fd[1] > 0){
                // Redirect stdout to input 
                dup2(fd[1], 1);
                // close both ends
                close(fd[0]);
                close(fd[1]);
            } else if (out > 0){
                // Redirect output
                dup2(out, 1);
                close(out);
            }
            // execute command
            int x = execve(argv[0], argv, environ);

            // if command not found, notify
            if(x < 0){
                printf("%s: Command not found\n",argv[0]);
                exit(1);
            }
        }

        // if pipe, execute following command
        if(argvPipe != NULL){
            pidPipe = fork();
            if(pidPipe < 0){
                unix_error("fork 2");
            }
            if (pidPipe == 0) {
                // unblock for execution
                sigprocmask(SIG_UNBLOCK, &set, NULL);            

                // Put child in process group with last command in pipe
                setpgid(pid,pid);
                
                // get input for second command from previous pipe
                dup2(fd[0], 0);
                // close both ends
                close(fd[0]);
                close(fd[1]);
                
                // redirect output
                if (out > 0){
                    dup2(out, 1);
                    close(out);
                }

                // execute command
                int x = execve(argvPipe[0], argvPipe, environ);

                // if command not found, notify
                if(x < 0){
                    printf("%s: Command not found\n",argvPipe[0]);
                    exit(1);
                }
            }
        }

        // close unclosed pipes
        if(fd[0]>0) {
            close(fd[0]);
        }
        if(fd[1]>0) {
            close(fd[1]);
        } 
        if(out > 0) {
            close(out);
        }

        // add job to jobs and unblock
        if(bg == 0){
            addjob(jobs, pid, FG, cmdline);
            // unblock now that job has been added and race condition avoided
            sigprocmask(SIG_UNBLOCK, &set, NULL);
            // wait for foreground job to complete
            waitfg(pid);
        } else {
            addjob(jobs, pid, BG, cmdline);
            // unblock now that job has been added and race condition avoided
            sigprocmask(SIG_UNBLOCK, &set, NULL);
            printf("[%d] (%d) %s", pid2jid(pid), pid, cmdline);
        }

    }
    return;
}

/* 
 * parseline - Parse the command line and build the argv array.
 * 
 * Characters enclosed in single quotes are treated as a single
 * argument.  Return true if the user has requested a BG job, false if
 * the user has requested a FG job.  
 */
int parseline(const char *cmdline, char **argv, int *argc) 
{
    // added a parameter to return count of arguments
    static char array[MAXLINE]; /* holds local copy of command line */
    char *buf = array;          /* ptr that traverses command line */
    char *delim;                /* points to first space delimiter */
    int counter;                   /* number of args */
    int bg;                     /* background job? */

    strcpy(buf, cmdline);
    buf[strlen(buf)-1] = ' ';  /* replace trailing '\n' with space */
    while (*buf && (*buf == ' ')) /* ignore leading spaces */
	buf++;

    /* Build the argv list */
    counter = 0;
    if (*buf == '\'') {
	buf++;
	delim = strchr(buf, '\'');
    }
    else {
	delim = strchr(buf, ' ');
    }

    while (delim) {
	argv[counter++] = buf;
	*delim = '\0';
	buf = delim + 1;
	while (*buf && (*buf == ' ')) /* ignore spaces */
	       buf++;

	if (*buf == '\'') {
	    buf++;
	    delim = strchr(buf, '\'');
	}
	else {
	    delim = strchr(buf, ' ');
	}
    }
    argv[counter] = NULL;
    
    if (counter == 0)  /* ignore blank line */
	return 1;

    /* should the job run in the background? */
    if ((bg = (*argv[counter-1] == '&')) != 0) {
	argv[--counter] = NULL;
    }
    (*argc) = counter;
    return bg;
}

/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 *    it immediately.  
 */
int builtin_cmd(char **argv) 
{
    if(argv[0] != NULL){
        // if the first term is quit, quit shell
        if(strcmp(argv[0], "quit")==0){
            // kill and reap children 
            sigchld_handler(1);
            exit(0);
        }
        // if first term is jobs, list all background jobs
        if(strcmp(argv[0], "jobs")==0){
            listjobs(jobs);
            return 1;
        }
        // if first term is bg or fg call do_bgfg
        if((strcmp(argv[0], "bg")==0) || (strcmp(argv[0], "fg")==0)){
            do_bgfg(argv);
            return 1;
        }
    }

    return 0;     /* not a builtin command */
}

/* 
 * do_bgfg - Execute the builtin bg and fg commands
 */
void do_bgfg(char **argv) 
{
    struct job_t *job;
    int jid;
    int pid;

    // if error in command, notify
    if(argv[1] == NULL) {
        printf("%s command requires PID or %%jobid argument\n", argv[0]);
        return;
    } 

    // get job, pid and jid from command line
    if (argv[1][0] == '%'){
        // get job with jid
        jid = atoi(argv[1]+1);
        job = getjobjid(jobs, jid);

        // if can't find job, notify
        if(job == NULL){
            printf("%s: No such job\n", argv[1]);
            return;
        } 
        pid = job->pid;
        
    } else if (isdigit(argv[1][0])) {
        // get job with pid
        pid = atoi(argv[1]);
        job = getjobpid(jobs, pid);

        // if can't find job, notify
        if(job == NULL){
            printf("(%s): No such process\n", argv[1]);
            return;
        } 
        jid = job->jid;

    } else {
        // if wrong format, notify
        printf("%s: argument must be a PID or %%jobid\n", argv[0]);
        return;
    }    

    // restart job by sending SIGCONT signal
    kill(pid, SIGCONT);

    // change state of job
    if(strcmp(argv[0], "bg")==0){
        // run in background
        job->state = BG;
        printf("[%d] (%d) %s", jid, pid, job->cmdline);

    } else {
        // run in foreground
        job->state = FG;
        // wait for foreground job to complete
        waitfg(pid);
    }
    return;
}

/* 
 * waitfg - Block until process pid is no longer the foreground process
 */
void waitfg(pid_t pid)
{
    // busy wait to temporarily suspend the process  while waiting for pid to end
    while(pid == fgpid(jobs)){
        sleep(1);
    }
    return;
}

/*****************
 * Signal handlers
 *****************/

/* 
 * sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
 *     a child job terminates (becomes a zombie), or stops because it
 *     received a SIGSTOP or SIGTSTP signal. The handler reaps all
 *     available zombie children, but doesn't wait for any other
 *     currently running children to terminate.  
 */
void sigchld_handler(int sig) 
{
    pid_t pid;
    int status;
    // request status info
    // do not wait if there is no child process ready (WNOHANG)
    // report on child processes stopped and terminated (WUNTRACED)
    while ((pid = waitpid(-1, &status, WNOHANG|WUNTRACED)) > 0) {
        if (WIFSTOPPED(status)){
            // child stopped
            int sig = WSTOPSIG(status);
            fprintf(stderr, "Job [%d] (%d) stopped by signal %d\n", pid2jid(pid), pid, sig);
            sigtstp_handler(sig);
            return;
        } else if(WIFSIGNALED(status)){
            // child terminated for uncaught signal
            int sig = WTERMSIG(status);
            fprintf(stderr, "Job [%d] (%d) terminated by signal %d\n", pid2jid(pid), pid, sig);
        } 
        // Reap jobs which are terminated or exited and not deleted
        deletejob(jobs, pid);
    }

    return;
}

/* 
 * sigint_handler - The kernel sends a SIGINT to the shell whenver the
 *    user types ctrl-c at the keyboard.  Catch it and send it along
 *    to the foreground job.  
 */
void sigint_handler(int sig) 
{
    // get foreground job id
    pid_t pid;
    pid = fgpid(jobs);
    // if there is foregrounf job, signal its process group
    if(pid){
        kill(pid,SIGINT);
    }
    return;
}

/*
 * sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
 *     the user types ctrl-z at the keyboard. Catch it and suspend the
 *     foreground job by sending it a SIGTSTP.  
 */
void sigtstp_handler(int sig) 
{
    // get foreground job id
    pid_t pid;
    pid = fgpid(jobs);
    // if there is foregrounf job, suspend it
    if(pid){
        kill(pid, SIGTSTP);
        struct job_t *job = getjobpid(jobs, pid);
        job->state = ST;
    }
    return;
}

/*********************
 * End signal handlers
 *********************/

/***********************************************
 * Helper routines that manipulate the job list
 **********************************************/

/* clearjob - Clear the entries in a job struct */
void clearjob(struct job_t *job) {
    job->pid = 0;
    job->jid = 0;
    job->state = UNDEF;
    job->cmdline[0] = '\0';
}

/* initjobs - Initialize the job list */
void initjobs(struct job_t *jobs) {
    int i;

    for (i = 0; i < MAXJOBS; i++)
	clearjob(&jobs[i]);
}

/* maxjid - Returns largest allocated job ID */
int maxjid(struct job_t *jobs) 
{
    int i, max=0;

    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].jid > max)
	    max = jobs[i].jid;
    return max;
}

/* addjob - Add a job to the job list */
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline) 
{
    int i;
    
    if (pid < 1)
	return 0;

    for (i = 0; i < MAXJOBS; i++) {
	if (jobs[i].pid == 0) {
	    jobs[i].pid = pid;
	    jobs[i].state = state;
	    jobs[i].jid = nextjid++;
	    if (nextjid > MAXJOBS)
		nextjid = 1;
	    strcpy(jobs[i].cmdline, cmdline);
  	    if(verbose){
	        printf("Added job [%d] %d %s\n", jobs[i].jid, jobs[i].pid, jobs[i].cmdline);
            }
            return 1;
	}
    }
    printf("Tried to create too many jobs\n");
    return 0;
}

/* deletejob - Delete a job whose PID=pid from the job list */
int deletejob(struct job_t *jobs, pid_t pid) 
{
    int i;

    if (pid < 1)
	return 0;

    for (i = 0; i < MAXJOBS; i++) {
	if (jobs[i].pid == pid) {
	    clearjob(&jobs[i]);
	    nextjid = maxjid(jobs)+1;
	    return 1;
	}
    }
    return 0;
}

/* fgpid - Return PID of current foreground job, 0 if no such job */
pid_t fgpid(struct job_t *jobs) {
    int i;

    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].state == FG)
	    return jobs[i].pid;
    return 0;
}

/* getjobpid  - Find a job (by PID) on the job list */
struct job_t *getjobpid(struct job_t *jobs, pid_t pid) {
    int i;

    if (pid < 1)
	return NULL;
    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].pid == pid)
	    return &jobs[i];
    return NULL;
}

/* getjobjid  - Find a job (by JID) on the job list */
struct job_t *getjobjid(struct job_t *jobs, int jid) 
{
    int i;

    if (jid < 1)
	return NULL;
    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].jid == jid)
	    return &jobs[i];
    return NULL;
}

/* pid2jid - Map process ID to job ID */
int pid2jid(pid_t pid) 
{
    int i;

    if (pid < 1)
	return 0;
    for (i = 0; i < MAXJOBS; i++)
	if (jobs[i].pid == pid) {
            return jobs[i].jid;
        }
    return 0;
}

/* listjobs - Print the job list */
void listjobs(struct job_t *jobs) 
{
    int i;
    
    for (i = 0; i < MAXJOBS; i++) {
	if (jobs[i].pid != 0) {
	    printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
	    switch (jobs[i].state) {
		case BG: 
		    printf("Running ");
		    break;
		case FG: 
		    printf("Foreground ");
		    break;
		case ST: 
		    printf("Stopped ");
		    break;
	    default:
		    printf("listjobs: Internal error: job[%d].state=%d ", 
			   i, jobs[i].state);
	    }
	    printf("%s", jobs[i].cmdline);
	}
    }
}
/******************************
 * end job list helper routines
 ******************************/


/***********************
 * Other helper routines
 ***********************/

/*
 * usage - print a help message
 */
void usage(void) 
{
    printf("Usage: shell [-hvp]\n");
    printf("   -h   print this message\n");
    printf("   -v   print additional diagnostic information\n");
    printf("   -p   do not emit a command prompt\n");
    exit(1);
}

/*
 * unix_error - unix-style error routine
 */
void unix_error(char *msg)
{
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

/*
 * app_error - application-style error routine
 */
void app_error(char *msg)
{
    fprintf(stdout, "%s\n", msg);
    exit(1);
}

/*
 * Signal - wrapper for the sigaction function
 */
handler_t *Signal(int signum, handler_t *handler) 
{
    struct sigaction action, old_action;

    action.sa_handler = handler;  
    sigemptyset(&action.sa_mask); /* block sigs of type being handled */
    action.sa_flags = SA_RESTART; /* restart syscalls if possible */

    if (sigaction(signum, &action, &old_action) < 0)
	unix_error("Signal error");
    return (old_action.sa_handler);
}

/*
 * sigquit_handler - The driver program can gracefully terminate the
 *    child shell by sending it a SIGQUIT signal.
 */
void sigquit_handler(int sig) 
{
    printf("Terminating after receipt of SIGQUIT signal\n");
    exit(1);
}



